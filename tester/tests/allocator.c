#define TEST_SMART_MMAP

#include "test.h"

#include <assert.h>

#define HEAP_SIZE 16

DEFINE_MMAP_IMPL(success) {
    base_mmap_checks(addr, length, prot, flags, fd, offset);
    return mmap(addr, length, prot, flags, fd, offset);
}

// success memory allocation
DEFINE_TEST(success_memalloc) {
  void* heap = heap_init(HEAP_SIZE);
  assert(heap);

  void* mem = _malloc(HEAP_SIZE / 2);
  assert(mem);
}

// free one block
DEFINE_TEST(free_one_block) {
  void* heap = heap_init(HEAP_SIZE);

  struct block_header* blocks[8];

  for (size_t i = 0; i < 8; i++) {
      blocks[i] = memalloc(HEAP_SIZE / 16, heap);
  }

  _free(blocks[3]->contents);
  assert(blocks[3]->is_free);
}

// free two block
DEFINE_TEST(free_two_blocks) {
  void* heap = heap_init(HEAP_SIZE);

  struct block_header* blocks[8];

  for (size_t i = 0; i < 8; i++) {
      blocks[i] = memalloc(HEAP_SIZE / 16, heap);
  }

  _free(blocks[4]->contents);
  assert(blocks[4]->is_free);

  _free(blocks[3]->contents);
  assert(blocks[3]->is_free);

  assert(blocks[3]->next == blocks[5]);
}

// memory out, new memory region expands old
DEFINE_TEST(out_memory) {
  void* heap = heap_init(HEAP_SIZE);

  _malloc(HEAP_SIZE * 2);
  void* mem = _malloc(HEAP_SIZE / 2);
  assert(mem);
}

// memory out, new memory region in new place
DEFINE_TEST(out_memory_and_region) {
  void* heap = heap_init(HEAP_SIZE);

  struct block_header* block = memalloc(3 * HEAP_SIZE / 4, heap);

  (void) mmap(block->contents + block->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

  void* mem = _malloc(HEAP_SIZE / 2);
  assert(mem);
}

int main() {
    current_mmap_impl = MMAP_IMPL(success);
    RUN_SINGLE_TEST(success_memalloc);
    RUN_SINGLE_TEST(free_one_block);
    RUN_SINGLE_TEST(free_two_blocks);
    RUN_SINGLE_TEST(out_memory);
    RUN_SINGLE_TEST(out_memory_and_region);
    return 0;
}
